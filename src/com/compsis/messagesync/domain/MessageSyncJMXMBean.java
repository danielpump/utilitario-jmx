/*
 * COMPSIS – Computadores e Sistemas Ind. e Com. LTDA<br>
 * Produto ${product_name} - ${product_description}<br>
 *
 * Data de Criação: 07/10/2014<br>
 * <br>
 * Todos os direitos reservados.
 */

package com.compsis.messagesync.domain;

import javax.management.ObjectName;

/** 
 * DOCUMENTAÇÃO DA CLASSE <br>
 * ---------------------- <br>
 * FINALIDADE: <br>
 * Define interface JMX para o Message Sync
 * <br>
 * HISTÓRICO DE DESENVOLVIMENTO: <br>
 * 07/10/2014 - @author Lucas Israel - Primeira versão da classe. <br>
 *<br>
 *<br>
 * LISTA DE CLASSES INTERNAS: <br>
 */

public interface MessageSyncJMXMBean {
	/**
	 * Define o {@link ObjectName} da operacao
	 * @return
	 */
	String getObjectName();
	
	/**
	 * Informa o {@link ClassLoader} padrão da aplicação para ser utilizador nas requisições JMX
	 * @param aClassLoader
	 */
	void setDefaultApplicationClassLoader(final ClassLoader aClassLoader);
}
