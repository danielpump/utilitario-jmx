/*
 * COMPSIS � Computadores e Sistemas Ind. e Com. LTDA<br>
 * TODO Produto $(product_name} - ${product_description}<br>
 *
 * Data de Cria��o: 24/03/2015<br>
 * <br>
 * Todos os direitos reservados.
 */

package com.compsis.utilitario.realizador;

import java.io.IOException;
import java.net.MalformedURLException;

import javax.management.JMX;
import javax.management.MBeanServerConnection;
import javax.management.MalformedObjectNameException;
import javax.management.ObjectName;

import br.com.compsis.autorizador.core.jmx.ISistemaMBean;

import com.compsis.utilitario.util.JMXUtil48;
import com.compsis.utilitario.util.Logger4JWhyNot;

/** 
 * DOCUMENTA��O DA CLASSE <br>
 * ---------------------- <br>
 * FINALIDADE: <br>
 * TODO Definir documenta��o da classe. <br>
 * <br>
 * HIST�RICO DE DESENVOLVIMENTO: <br>
 * 24/03/2015 - @author Daniel Ferraz - Primeira vers�o da classe. <br>
 *<br>
 *<br>
 * LISTA DE CLASSES INTERNAS: <br>
 */

public class DesligarAutorizador implements Realizador {
	
	private String nomeDoHost;
	
	public DesligarAutorizador(String nomeDoHost) {
		this.nomeDoHost = nomeDoHost;
	}

	@Override
	public void realizar() throws Exception {
		
		MBeanServerConnection syncMbsc = gerarConexaoSync();
		
		ObjectName syncMBeanName = gerarNomeObjectSync();
		
		Logger4JWhyNot.debug("Conectando no objeto do autorizador");
		ISistemaMBean sistemaBean = JMX.newMBeanProxy(syncMbsc, syncMBeanName, ISistemaMBean.class, true);
		
		Logger4JWhyNot.debug("Desligando o sistema");
		sistemaBean.desligarSistema();
		
		Logger4JWhyNot.log("Sistema do host, foi desligado com sucesso." + nomeDoHost);
		
	}
	
	private ObjectName gerarNomeObjectSync()
			throws MalformedObjectNameException {
		String nomeOBjectSync = "br.com.compsis.autorizador:type=Sistema,name=Sistema";
		Logger4JWhyNot.debug("Criando obejto sync: " + nomeOBjectSync);
		ObjectName syncMBeanName = new ObjectName(nomeOBjectSync);
		Logger4JWhyNot.debug("Criando obejto sync: " + nomeOBjectSync);
		return syncMBeanName;
	}
	

	private MBeanServerConnection gerarConexaoSync() throws MalformedURLException,
			IOException {
		return JMXUtil48.gerarConexaoSync("service:jmx:rmi:///jndi/rmi://"+ this.nomeDoHost +":5000/jmxrmi");
	}


}
