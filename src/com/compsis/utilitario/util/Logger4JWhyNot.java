/*
 * COMPSIS � Computadores e Sistemas Ind. e Com. LTDA<br>
 * TODO Produto $(product_name} - ${product_description}<br>
 *
 * Data de Cria��o: 24/03/2015<br>
 * <br>
 * Todos os direitos reservados.
 */

package com.compsis.utilitario.util;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

/** 
 * DOCUMENTA��O DA CLASSE <br>
 * ---------------------- <br>
 * FINALIDADE: <br>
 * TODO Definir documenta��o da classe. <br>
 * <br>
 * HIST�RICO DE DESENVOLVIMENTO: <br>
 * 24/03/2015 - @author Daniel Ferraz - Primeira vers�o da classe. <br>
 *<br>
 *<br>
 * LISTA DE CLASSES INTERNAS: <br>
 */

public class Logger4JWhyNot {
	
	private static FileWriter arquivoLog;
	private static FileWriter arquivoLogDebug;
	
	static{
		try {
			arquivoLog = new FileWriter(new File("logUtilitarioJMX"+ System.currentTimeMillis() + ".log"));
			arquivoLogDebug = new FileWriter(new File("logDebugUtilitarioJMX"+ System.currentTimeMillis() + ".log"));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public static void log(String mensagem){
		try {
			arquivoLog.write(mensagem);
			arquivoLog.write("\r\n");
			arquivoLog.flush();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static void debug(String mensagem){
		try {
			arquivoLogDebug.write(mensagem);
			arquivoLogDebug.write("\r\n");
			arquivoLogDebug.flush();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
