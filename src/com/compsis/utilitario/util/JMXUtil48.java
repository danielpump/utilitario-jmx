/*
 * COMPSIS � Computadores e Sistemas Ind. e Com. LTDA<br>
 * TODO Produto $(product_name} - ${product_description}<br>
 *
 * Data de Cria��o: 24/03/2015<br>
 * <br>
 * Todos os direitos reservados.
 */

package com.compsis.utilitario.util;

import java.io.IOException;
import java.net.MalformedURLException;

import javax.management.MBeanServerConnection;
import javax.management.remote.JMXConnector;
import javax.management.remote.JMXConnectorFactory;
import javax.management.remote.JMXServiceURL;

/**
 * DOCUMENTA��O DA CLASSE <br>
 * ---------------------- <br>
 * FINALIDADE: <br>
 * TODO Definir documenta��o da classe. <br>
 * <br>
 * HIST�RICO DE DESENVOLVIMENTO: <br>
 * 24/03/2015 - @author Daniel Ferraz - Primeira vers�o da classe. <br>
 * <br>
 * <br>
 * LISTA DE CLASSES INTERNAS: <br>
 */

public class JMXUtil48 {

	public static MBeanServerConnection gerarConexaoSync(String url) throws MalformedURLException, IOException {
		Logger4JWhyNot.debug("Conectando em: " + url);
		JMXServiceURL jmxUrl = new JMXServiceURL(url);
		JMXConnector jmxc = JMXConnectorFactory.connect(jmxUrl, null);
		MBeanServerConnection mbsc = jmxc.getMBeanServerConnection();
		Logger4JWhyNot.debug("Conectado em: " + url);
		return mbsc;
	}

}
