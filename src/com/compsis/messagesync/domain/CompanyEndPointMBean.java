/*
 * COMPSIS – Computadores e Sistemas Ind. e Com. LTDA<br>
 * Produto ${product_name} - ${product_description}<br>
 *
 * Data de Criação: 08/10/2014<br>
 * <br>
 * Todos os direitos reservados.
 */

package com.compsis.messagesync.domain;

/** 
 * DOCUMENTAÇÃO DA CLASSE <br>
 * ---------------------- <br>
 * FINALIDADE: <br>
 * Definição da interface MBean para controle de CompanyEndPoint via JMX
 * <br>
 * HISTÓRICO DE DESENVOLVIMENTO: <br>
 * 08/10/2014 - @author Lucas Israel - Primeira versão da classe. <br>
 *<br>
 *<br>
 * LISTA DE CLASSES INTERNAS: <br>
 */

public interface CompanyEndPointMBean extends MessageSyncJMXMBean {
	
	/**
	 * Obtem o nome do {@link CompanyEndPoint}
	 * @return
	 */
	String getDescription();
	
	
	/**
	 * Nome da fila de requisições de tags
	 * @return
	 */
	String getSeqTagsDestination();
	
	/**
	 * Nome da fila de tags
	 * @return
	 */
	String getTagsDestination();
	
	/**
	 * Nome da fila de processamento da OSA para o {@link CompanyEndPoint}
	 * @return
	 */
	String getTagsManagerDestination();
	
	/**
	 * Nome da fila de processamento de alta prioridade para o {@link CompanyEndPoint}
	 * @return
	 */
	String getTagsPriorityDestination();
	
	/**
	 * Série do {@link CompanyEndPoint}
	 * @return
	 */
	Integer getSerie();
	
	/**
	 * Sequencial do {@link CompanyEndPoint}
	 * @return
	 */
	Long getInitialSequential();
	
	/**
	 * Número da OSA para o {@link CompanyEndPoint}
	 * @return
	 */
	Integer getManagerIdentification();
	
	/**
	 * Altera o semafaro para processamento de alta prioridades 
	 */
	void changeSemaphoreToPriority();
	
	/**
	 * Altera o semafaro para processamento de tags da OSA
	 */
	void changeSemaphoreToManager();
	
	/**
	 * Simula o processo de solicitação de TAGs para o {@link CompanyEndPoint}
	 * @param sequential
	 * @param incrementSerie
	 */
	void tagRequest(long sequential, boolean incrementSerie);

	
	/** 
	 * Atualiza a serie do endPoint para o numero passado como parametro.
	 * @param serie
	 */
	void updateSerieTo(Integer serie);

	/** 
	 * Atualiza o sequencial do endPoint para o numero passado como parametro.
	 * @param sequential
	 */
	void updateSequentialTo(Integer sequential);
	
	/**
	 * Aborta o carregamento e envio de mensagem em execução
	 */
	void stopLoadAndSendMessageProcess();
}
