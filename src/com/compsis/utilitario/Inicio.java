/*
 * COMPSIS � Computadores e Sistemas Ind. e Com. LTDA<br>
 * TODO Produto $(product_name} - ${product_description}<br>
 *
 * Data de Cria��o: 23/03/2015<br>
 * <br>
 * Todos os direitos reservados.
 */

package com.compsis.utilitario;

import java.io.IOException;
import java.lang.reflect.UndeclaredThrowableException;
import java.rmi.UnknownHostException;

import com.compsis.utilitario.realizador.ZerarSequencialPistas;
import com.compsis.utilitario.util.Logger4JWhyNot;


/** 
 * DOCUMENTA��O DA CLASSE <br>
 * ---------------------- <br>
 * FINALIDADE: <br>
 * TODO Definir documenta��o da classe. <br>
 * <br>
 * HIST�RICO DE DESENVOLVIMENTO: <br>
 * 23/03/2015 - @author Daniel Ferraz - Primeira vers�o da classe. <br>
 *<br>
 *<br>
 * LISTA DE CLASSES INTERNAS: <br>
 */

public class Inicio {
	
	public static void main(String[] args) throws Exception {	
		
			String numeroPracaInicial = args[0];
			String numeroPracaFinal = args[1];

			String numeroPistaInicial = args[2];
			String numeroPistaFinal = args[3];
			
			String osaNumero = args[4];
			
			Logger4JWhyNot.log("Parametro praca inicio : " + numeroPracaInicial);
			Logger4JWhyNot.log("Parametro praca final  : " + numeroPracaFinal);
			Logger4JWhyNot.log("Parametro pista inicio : " + numeroPistaInicial);
			Logger4JWhyNot.log("Parametro pista final  : " + numeroPistaFinal);
			Logger4JWhyNot.log("Parametro numero osa   : " + osaNumero);
			
			for (int iniPraca = Integer.parseInt(numeroPracaInicial) ; iniPraca <= Integer.parseInt(numeroPracaFinal) ;iniPraca++) {				
				for (int iniPista = Integer.parseInt(numeroPistaInicial) ; iniPista <= Integer.parseInt(numeroPistaFinal) ;iniPista++) {
					try{
						
					ZerarSequencialPistas zerar = new ZerarSequencialPistas(iniPraca, iniPista, osaNumero);	
					
					zerar.realizar();
					
					} catch (UndeclaredThrowableException instanceException){
						Logger4JWhyNot.log("Objeto com nome, n�o foi encontrado: " + instanceException.getCause().getMessage());
					} catch (UnknownHostException hostException){
						Logger4JWhyNot.log("Host desconhecido: " + hostException.getMessage());
					} catch (IOException hostException){
						Logger4JWhyNot.log("Host n�o conectou: " + hostException.getMessage());
					} catch (Exception exception){
						exception.printStackTrace();
					} catch (Throwable throwable){
						throwable.printStackTrace();
					}
					
					
				}				
			}
			
	}



	
	
	


}
