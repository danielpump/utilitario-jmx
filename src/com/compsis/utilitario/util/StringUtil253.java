/*
 * COMPSIS � Computadores e Sistemas Ind. e Com. LTDA<br>
 * TODO Produto $(product_name} - ${product_description}<br>
 *
 * Data de Cria��o: 24/03/2015<br>
 * <br>
 * Todos os direitos reservados.
 */

package com.compsis.utilitario.util;

/** 
 * DOCUMENTA��O DA CLASSE <br>
 * ---------------------- <br>
 * FINALIDADE: <br>
 * TODO Definir documenta��o da classe. <br>
 * <br>
 * HIST�RICO DE DESENVOLVIMENTO: <br>
 * 24/03/2015 - @author Daniel Ferraz - Primeira vers�o da classe. <br>
 *<br>
 *<br>
 * LISTA DE CLASSES INTERNAS: <br>
 */

public class StringUtil253 {
	
	public static String preencheZeroEsquerda(int codigo, int quantZero) {
		
		String codigoTexto = Integer.toString(codigo);
		
		for (int i = codigoTexto.length(); i < quantZero; i++) {
			codigoTexto = "0" + codigoTexto;
		}
		
		return codigoTexto;
	}

}
