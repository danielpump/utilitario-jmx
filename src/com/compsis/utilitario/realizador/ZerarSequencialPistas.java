/*
 * COMPSIS � Computadores e Sistemas Ind. e Com. LTDA<br>
 * TODO Produto $(product_name} - ${product_description}<br>
 *
 * Data de Cria��o: 24/03/2015<br>
 * <br>
 * Todos os direitos reservados.
 */

package com.compsis.utilitario.realizador;

import java.io.IOException;
import java.net.MalformedURLException;

import javax.management.JMX;
import javax.management.MBeanServerConnection;
import javax.management.MalformedObjectNameException;
import javax.management.ObjectName;

import br.com.compsis.autorizador.core.jmx.IMOPMBean;

import com.compsis.messagesync.domain.CompanyEndPointMBean;
import com.compsis.utilitario.util.JMXUtil48;
import com.compsis.utilitario.util.Logger4JWhyNot;
import com.compsis.utilitario.util.StringUtil253;

/** 
 * DOCUMENTA��O DA CLASSE <br>
 * ---------------------- <br>
 * FINALIDADE: <br>
 * TODO Definir documenta��o da classe. <br>
 * <br>
 * HIST�RICO DE DESENVOLVIMENTO: <br>
 * 24/03/2015 - @author Daniel Ferraz - Primeira vers�o da classe. <br>
 *<br>
 *<br>
 * LISTA DE CLASSES INTERNAS: <br>
 */

public class ZerarSequencialPistas implements Realizador {
	
	String codigoPraca;
	String codigoPista;
	String osaNumero;
	
	
	public ZerarSequencialPistas(int numPraca, int numPista, String osaNumero) {
		this.codigoPraca = StringUtil253.preencheZeroEsquerda(numPraca, 3);
		this.codigoPista = StringUtil253.preencheZeroEsquerda(numPista, 2);
		this.osaNumero = osaNumero;
	}


	@Override
	public void realizar() throws Exception {

		MBeanServerConnection authMbsc = carregarConexaoAuth(codigoPraca, codigoPista);					
		ObjectName authMBeanName = gerarNomeObjetoAuth(osaNumero,codigoPraca, codigoPista);
		
		MBeanServerConnection syncMbsc = gerarConexaoSync();
		
		ObjectName syncMBeanName = gerarNomeObjectSync();
		
		Logger4JWhyNot.debug("Conectando no objeto auth");
		IMOPMBean authMopBean = JMX.newMBeanProxy(authMbsc, authMBeanName, IMOPMBean.class, true);
		
		Logger4JWhyNot.debug("Conectando no objeto sync");
		CompanyEndPointMBean syncCompanyBean = JMX.newMBeanProxy(syncMbsc, syncMBeanName, CompanyEndPointMBean.class, true);
		
		Logger4JWhyNot.debug("Zerando sequencial");
		authMopBean.alterarSequencialPara(-1l);
		
		Logger4JWhyNot.debug("Fazendo requisi��o zero");
		syncCompanyBean.tagRequest(-1, false);
		
		Logger4JWhyNot.log(">>>>>>>>>>>>>>>>>>>>>>Atualizado sequencial na pista AUTORIZADOR_PR" + codigoPraca + "V" + codigoPista + " para a OSA" + osaNumero);
		Logger4JWhyNot.debug(">>>>>>>>>>>>>>>>>>>>>>Atualizado sequencial na pista AUTORIZADOR_PR" + codigoPraca + "V" + codigoPista + " para a OSA" + osaNumero);
		
	}


	private ObjectName gerarNomeObjectSync()
			throws MalformedObjectNameException {
		String nomeOBjectSync = "com.compsis.messagesync.jmx:group=configuration,type=companyendpoint,name=PR" + codigoPraca + "V" + codigoPista + "." + osaNumero;
		Logger4JWhyNot.debug("Criando obejto sync: " + nomeOBjectSync);
		ObjectName syncMBeanName = new ObjectName(nomeOBjectSync);
		Logger4JWhyNot.debug("Criando obejto sync: " + nomeOBjectSync);
		return syncMBeanName;
	}
	

	private static MBeanServerConnection gerarConexaoSync() throws MalformedURLException,
			IOException {
		return JMXUtil48.gerarConexaoSync("service:jmx:rmi:///jndi/rmi://SINCRONIZADOR:55555/jmxrmi");
	}

	private static ObjectName gerarNomeObjetoAuth(String osaNumero,
			String codigoPraca, String codigoPista)
			throws MalformedObjectNameException {
		String nomeDoObjeto = "br.com.compsis.autorizador:group=Autorizadores,type=Detalhado,name=PR" + codigoPraca + "V" + codigoPista + "_OSA" + osaNumero;
		Logger4JWhyNot.debug("Criando obejto auth: " + nomeDoObjeto);
		ObjectName authMBeanName = new ObjectName(nomeDoObjeto);
		Logger4JWhyNot.debug("Objeto criado auth: " + nomeDoObjeto);
		return authMBeanName;
	}

	private static MBeanServerConnection carregarConexaoAuth(
			String codigoPraca, String codigoPista)
			throws MalformedURLException, IOException {		
		return JMXUtil48.gerarConexaoSync("service:jmx:rmi:///jndi/rmi://AUTORIZADOR_PR" + codigoPraca + "V" + codigoPista + ":5000/jmxrmi");
	}

}
